﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Services.Description;
using System.Windows.Forms;
//using Visitor_Manage_.Visitor_List;
namespace Visitor_Manage_
{
   public static class Busness
    {

        private static ICredentials _credential;
        public static string VisitorNo;
        public static string Status;
        public static ICredentials ICredentials()
        {
            string NUserName= Properties.Settings.Default.NavUserName.ToString();
            string Npassword = Properties.Settings.Default.NavPassword.ToString();
           string NDomain= Properties.Settings.Default.NavDomain.ToString();
            
            return new NetworkCredential(NUserName,Npassword, NDomain);
        }
        public static bool ValidateQrCode(string VID)
        {
            try
            {
                Visitor_List.VGP_Service ss = new Visitor_List.VGP_Service();
                ss.Credentials = ICredentials();

                List<Visitor_List.VGP_Filter> L = new List<Visitor_List.VGP_Filter>();
                Visitor_List.VGP_Filter VLF = new Visitor_List.VGP_Filter();
                VLF.Field = Visitor_List.VGP_Fields.Visitor_No;
                VLF.Criteria = VID;
                L.Add(VLF);

                Visitor_List.VGP[] Rec = ss.ReadMultiple(L.ToArray(), null, 1);
                if (Rec.Length > 0)
                {
                    foreach (Visitor_List.VGP v in Rec)
                    {

                        if (v.Visitor_No == VID)
                        {

                            VisitorNo = v.Visitor_No;
                            if (v.Status == Visitor_List.Status.Generated)
                            {
                                Status = Visitor_List.Status.Generated.ToString();
                            }
                            else if (v.Status == Visitor_List.Status.Visited)
                            {
                                Status = Visitor_List.Status.Visited.ToString();
                            }
                            else
                            {
                                Status = Visitor_List.Status._blank_.ToString();
                            }

                            return true;
                        }
                        else
                        {
                            return false;

                        }
                    }

                }
            }
            catch (Exception es )
            {

                MessageBox.Show(es.Message);
            }

            
            return false;

        }


        public static void cHECKoUT(string VID)
        {

            
        }


        public static void SendToPrinter(string filePath)
        {
            PdfDocument doc = new PdfDocument();
            doc.LoadFromFile(filePath);
            doc.PrintDocument.Print();
            Thread.Sleep(2000);

            //ProcessStartInfo info = new ProcessStartInfo();
            //info.Verb = "print";
            //info.FileName = filePath;//@"C:\Erda New pr\Visitor_Manage_2992020\Visitor_Manage_\bin\Debug\file.pdf";
            //info.CreateNoWindow = true;
            //info.WindowStyle = ProcessWindowStyle.Hidden;

            //Process p = new Process();
            //p.StartInfo = info;
            //p.Start();

            //long ticks = -1;
            //while (ticks != p.TotalProcessorTime.Ticks)
            //{
            //    ticks = p.TotalProcessorTime.Ticks;
            //    Thread.Sleep(1000);
            //}

            //if (false == p.CloseMainWindow())
            //    p.Kill();
        }

    }
}
