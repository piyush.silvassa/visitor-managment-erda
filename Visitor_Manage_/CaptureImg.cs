﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Visitor_Manage_.SendMailTmpCust;
namespace Visitor_Manage_
{
    public partial class CaptureImg : Form
    {
        System.Windows.Forms.Timer MyTimer = new System.Windows.Forms.Timer();
        VideoCapture capture;
        Mat frame;
        Bitmap image;
        private Thread camera;
        bool isCameraRunning = false;
        

        public CaptureImg()
        {
            InitializeComponent();
        }

        private void CaptureImg_Load(object sender, EventArgs e)
        {
            panel1.Left = (this.ClientSize.Width - panel1.Width) / 2;
            panel1.Top = (this.ClientSize.Height - panel1.Height) / 2;
            camera = new Thread(new ThreadStart(CaptureCameraCallback));
            camera.Start();
            isCameraRunning = true;

           
            MyTimer.Interval = (1 * 60 * 1000); // 20 mins
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Image img = Image.FromFile(path);
            //MemoryStream tmpStream = new MemoryStream();
            //img.Save(tmpStream, ImageFormat.Png); // change to other format
            //tmpStream.Seek(0, SeekOrigin.Begin);
            //byte[] imgBytes = new byte[MAX_IMG_SIZE];
            //tmpStream.Read(imgBytes, 0, MAX_IMG_SIZE);

            if (button2.Text == "Retake")
            {
                camera = new Thread(new ThreadStart(CaptureCameraCallback));
                camera.Start();
                isCameraRunning = true;
                button2.Text = "Capture";
            }
            else if (isCameraRunning)
            {
                // Take snapshot of the current image generate by OpenCV in the Picture Box
                Bitmap snapshot = new Bitmap(pictureBox1.Image);

                // Save in some directory
                // in this example, we'll generate a random filename e.g 47059681-95ed-4e95-9b50-320092a3d652.png
                // snapshot.Save(@"C:\Users\sdkca\Desktop\mysnapshot.png", ImageFormat.Png);
                // snapshot.Save(string.Format(@"C:\Users\ajad\Downloads\{0}.png", Guid.NewGuid()), ImageFormat.Png);
                isCameraRunning = false;
                button2.Text = "Retake";
                isCameraRunning = false;
                capture.Dispose();
                camera.Abort();

            }
            else
            {
                Console.WriteLine("Cannot take picture if the camera isn't capturing image!");
            }
        }
        private void CaptureCameraCallback()
        {

            frame = new Mat();
            capture = new VideoCapture(0);
            capture.Open(0);

            if (capture.IsOpened())
            {
                while (isCameraRunning)
                {

                    capture.Read(frame);
                    image = BitmapConverter.ToBitmap(frame);
                    if (pictureBox1.Image != null)
                    {
                        ResetImg();
                    }
                   
                    SetIMG();
                }
            }
        }

        public void SetIMG()
        {

            this.Invoke((MethodInvoker)delegate
            {
                pictureBox1.Image = image;
            });
           
        }
        public void ResetImg()
        {
            this.Invoke((MethodInvoker)delegate
            {
               // pictureBox1.Image.Dispose();
            });

        }

        private void button1_Click(object sender, EventArgs e)
        {
             this.Enabled = false;
            var GetDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            try
            {
                
                string picture = Fun.ImageToBase64(pictureBox1.Image, System.Drawing.Imaging.ImageFormat.Png);

                SendMailTmpCust.Send_Mail_Tmpcust Se1 = new SendMailTmpCust.Send_Mail_Tmpcust();
                Se1.Credentials = Busness.ICredentials();
                Se1.SetPicture(Busness.VisitorNo, picture);



               // Test ts = new Test();
               // ts.UseDefaultCredentials = true;
               // ts.SetPicture("1000", picture);

                string fileName = "file.pdf";
                string fileNamePrt = "\\file.pdf";
                string ffpth = GetDirectory;
                if (File.Exists(Path.Combine(GetDirectory, fileName)))
                {
                    // If file found, delete it    
                    File.Delete(Path.Combine(GetDirectory, fileName));
                    Console.WriteLine("File deleted.");
                }


                string Convertbitstring = "";
                Se1.ReportPDF(ref Convertbitstring, Busness.VisitorNo);
                Byte[] bytesa = Convert.FromBase64String(Convertbitstring);
                System.IO.FileStream stream = new FileStream(GetDirectory + fileNamePrt, FileMode.CreateNew);
                System.IO.BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(bytesa, 0, bytesa.Length);
                writer.Close();
                string path = GetDirectory + fileNamePrt;
                isCameraRunning = false;
                capture.Dispose();
                camera.Abort();

                Busness.SendToPrinter(path);

               // MessageBox.Show("Please Collect your Appoinment from Security cabin ");
                //Fun.VMsg("Please Collect your Appoinment from Security cabin ");
                Form1 f= new Form1();
                f.Close();
               
                this.Close();
            }
            catch (Exception ex)
            {
                this.Enabled = true;
                this.Close();
              
            }
            this.Enabled = true;


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            FRM_IN vds = new FRM_IN();
            vds.ShowDialog(this);
        }
        private void MyTimer_Tick(object sender, EventArgs e)
        {
           // MessageBox.Show("The form will now be closed.", "Time Elapsed");
            MyTimer.Stop();
            this.Hide();
            this.Close();
        }
    }
}
