﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace Visitor_Manage_
{
    class ExtraFun
    {
        public static string ExtraMsg="";

       public void SMessage(string MSG)
        {
            SpeechSynthesizer sp = new SpeechSynthesizer();
            sp.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult); // to change VoiceGender and VoiceAge check out those links below
            sp.Volume = 100;  // (0 - 100)
            sp.Rate = -1;     // (-10 - 10)
            sp.Speak(MSG+" " + ExtraMsg);
            // sp.SpeakAsync("Well come to ERDA",, SpeechVoiceSpeakFlags);// here args = pran
        }
    }
}
