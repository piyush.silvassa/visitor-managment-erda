﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
//using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Visitor_Manage_.Visitor_List;

namespace Visitor_Manage_
{
    public partial class FRM_IN : Form
    {
     //   System.Windows.Forms.Timer MyTimer = new System.Windows.Forms.Timer();
        Timer MyTimer = new Timer();
        string otpValidate;
        public FRM_IN()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleparam = base.CreateParams;
                handleparam.ExStyle |= 0x02000000;
                return handleparam;
            }
        }

        private void FRM_IN_Load(object sender, EventArgs e)
        {
           
            panel1.Left = (this.ClientSize.Width - panel1.Width) / 2;
            panel1.Top = (this.ClientSize.Height - panel1.Height) / 2;
            //Fun.VMsg("Please Scan Your QR CODE IF you Dont QR Code Then Please Enter appointment number ");
           
            MyTimer.Interval = 50000; // 45 mins
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();
            panel2.Visible = false;
            // Fun.VMsg("Please Scan Your QR CODE");
            // Fun.VMsg("IF you Dont Have QR Code Then Please Enter appointment number ");
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            
            try
            {
                if (!string.IsNullOrWhiteSpace(TxtRegNo.Text) )
                {
                    if (Busness.ValidateQrCode(TxtRegNo.Text))
                    {
                        if(Busness.Status== "Generated")
                        {
                            SendMailTmpCust.Send_Mail_Tmpcust Se1 = new SendMailTmpCust.Send_Mail_Tmpcust();
                            Se1.Credentials = Busness.ICredentials();
                            Se1.OutVisitor(Busness.VisitorNo);
                            LBError.Text = "Thanks for visiting us";
                            Fun.VMsg("Thanks for visiting us");                      
                            this.Hide();
                            this.Close();
                            return;
                        }
                        else if (Busness.Status == "_blank_")
                        {
                            try
                            {
                                SendMailTmpCust.Send_Mail_Tmpcust Se1 = new SendMailTmpCust.Send_Mail_Tmpcust();
                                Se1.Credentials = Busness.ICredentials();
                                Se1.INVisitor(Busness.VisitorNo);

                                this.Hide();
                                // this.Close();
                                FrmVisitorDetails vds = new FrmVisitorDetails();
                                vds.ShowDialog(this);
                                // LBError.Text = "Please collect report from Security Cabin";
                                //Fun.VMsg("Please collect report from Security Cabin");
                                // this.Hide();
                                //this.Close();
                                //return;

                                /*
                                 string MobileNo;
                                Visitor_List_Service ss = new Visitor_List_Service();
                                ss.Credentials = Busness.ICredentials();

                                List<Visitor_List_Filter> L = new List<Visitor_List_Filter>();
                                Visitor_List_Filter VLF = new Visitor_List_Filter();
                                VLF.Field = Visitor_List_Fields.Visitor_No;
                                VLF.Criteria = Busness.VisitorNo;
                                L.Add(VLF);

                                Visitor_List.Visitor_List[] Rec = ss.ReadMultiple(L.ToArray(), null, 1);
                                if (Rec.Length > 0)
                                {
                                    foreach (Visitor_List.Visitor_List v in Rec)
                                    {
                                        MobileNo = v.Contact_No;
                                        string OtppsStr = OTPStringGen();
                                        SendEnder(MobileNo, OtppsStr);
                                        panel2.Visible = true;
                                        panel1.Visible = false;
                                        label4.Text = "OTP has been Sent successfully on your Mobile No. : " + MobileNo;


                                    }

                                }     */

                            }
                            catch (Exception ex)
                            {

                            }


                            //this.Hide();
                            //// this.Close();
                            //FrmVisitorDetails vds = new FrmVisitorDetails();
                            //vds.ShowDialog(this);
                         
                        }
                        else
                        {
                           LBError.Text = "Your out entry has been already processed!!";
                           Fun.VMsg("Your out entry has been already processed!!");

                        }
                      
                    }
                    else
                    {
                        SystemSounds.Beep.Play();
                        LBError.Text = "This is not a valid appointment number";
                        Fun.VMsg("This is Not a Valid Appointment number"+"     "+ "Please Enter Valid appointment number");
                       
                    }


                }
                else
                {
                    SystemSounds.Beep.Play();
                    LBError.Text = "Please Enter Valid appointment number";
                    Fun.VMsg("this is Not a Valid Appointment number" + "     " + "Please Enter a Valid appointment number");
                   
                }

            }
            catch (Exception ex)
            {
                this.Enabled = true;
               

                MessageBox.Show(ex.Message);
            }
            this.Enabled = true;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyTimer.Stop();
            this.Hide();
            this.Close();
          
           
        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(TxtRegNo.Text))
            {
                Fun.VMsg("Please Scan Your QR CODE");
                Fun.VMsg("IF you Dont Have QR Code Then Please Enter appointment number ");
            }
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private string OTPStringGen()
        {
            var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            var stringChars1 = new char[6];
            var random1 = new Random();

            for (int i = 0; i < stringChars1.Length; i++)
            {
                stringChars1[i] = chars1[random1.Next(chars1.Length)];
            }

            return new String(stringChars1);

        }

        protected void SendEnder(string PhonNo, String OTPCode)
        {
            try
            {
                otpValidate = OTPCode;
                string Number = PhonNo;
                //string APIKey = ConfigurationManager.AppSettings["FDateFormate"].ToString();//This may vary api to api. like ite may be password, secrate key, hash etc
                //string UserName = ConfigurationManager.AppSettings["OtpuserName"].ToString();
                //string PassWord = ConfigurationManager.AppSettings["OtpPass"].ToString();
                string Message = "OTP to reset your password of ERDA Customer portal is '" + OTPCode + "' . Do not share with anyone.";
                //  string URL = ConfigurationManager.AppSettings["OtpLink"].ToString()+ UserName + "&hash=" + APIKey + "&sender=" + SenderName + "&numbers=" + Number + "&message=" + Message;
                string URL = " http://smslane.com/vendorsms/pushsms.aspx?user=erdasysadmin@erda.org&password=apit1974&msisdn=" + Number + "&sid=ERDAHO&msg=OTP to reset your password of ERDA Customer portal is '" + OTPCode + "' . Do not share with anyone.&fl=0&gwid=2";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(URL);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                string results = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {


            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if  (EnterOTP.Text != otpValidate)
            {
                Fun.VMsg("Please Enter Valid OTP");
            }
            else
            {
                this.Hide();
                // this.Close();
                FrmVisitorDetails vds = new FrmVisitorDetails();
                vds.ShowDialog(this);
            }


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            if (EnterOTP.Text != otpValidate)
            {
                LBError.Text = "Please Enter Valid OTP";
                Fun.VMsg("Please Enter Valid OTP");
            }
            else
            {
                this.Hide();
                // this.Close();
                FrmVisitorDetails vds = new FrmVisitorDetails();
                vds.ShowDialog(this);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        //private void TxtRegNo_TextChanged(object sender, EventArgs e)
       
        //{
           
        //}

        private void TxtRegNo_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

          ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));

        }

        private void TxtRegNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("hello");
             }
        }

        private void TxtRegNo_KeyDown_1(object sender, KeyEventArgs e)
        {

           // Thread.Sleep(3000);
            if (e.KeyCode == Keys.Enter)
            {
               
                string tmpreg = TxtRegNo.Text;
                TxtRegNo.Text = "";

              
                Char delimiter = (':');
                String[] substrings = tmpreg.Split(delimiter);
                string Number = substrings[1];
                char delimiter1 = (';');
                String[] substrings1 = Number.Split(delimiter1);
                string Visitor_Number = substrings1[0];

                TxtRegNo.Text = Visitor_Number;

                button2_Click((object) sender, (EventArgs) e);
            }

        }

        private void button3_Click_1(object sender, EventArgs e)
        {

        }
    }
}
