﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace Visitor_Manage_
{
   public static class Fun
    {
        public static void VMsg(string message1,string message2="",string message3 = "")
        {
            SpeechSynthesizer synthesizer = new SpeechSynthesizer();
            synthesizer.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult); // to change VoiceGender and VoiceAge check out those links below
            synthesizer.Volume = 100;  // (0 - 100)
            synthesizer.Rate = 0;     // (-10 - 10)
                                      // Synchronous
           // synthesizer.Speak("Now I'm speaking, no other function'll work");
            // Asynchronous
            synthesizer.Speak(message1+" "+message2+" "+message3); // here args = pran
        }

        public static void VMsgM(string message1, string message2 = "", string message3 = "")
        {
            SpeechSynthesizer synthesizer = new SpeechSynthesizer();
            synthesizer.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult); // to change VoiceGender and VoiceAge check out those links below
            synthesizer.Volume = 100;  // (0 - 100)
            synthesizer.Rate = 0;     // (-10 - 10)
                                      // Synchronous
                                      // synthesizer.Speak("Now I'm speaking, no other function'll work");
                                      // Asynchronous
            synthesizer.SpeakAsync(message1 + " " + message2 + " " + message3); // here args = pran
        }

        public static string ImageToBase64(System.Drawing.Image image,

  System.Drawing.Imaging.ImageFormat format)

        {

            using (MemoryStream ms = new MemoryStream())

            {

                // Convert Image to byte[]

                image.Save(ms, format);

                byte[] imageBytes = ms.ToArray();


                // Convert byte[] to Base64 String

                string base64String = Convert.ToBase64String(imageBytes);

                return base64String;

            }

        }

public static void SHOWLoader()
        {
            FrmProcess ff = new FrmProcess();
            ff.ShowDialog();
        }


        public static void HideLoader()
        {
            FrmProcess ff = new FrmProcess();
            ff.Hide();
        }

    }
}
