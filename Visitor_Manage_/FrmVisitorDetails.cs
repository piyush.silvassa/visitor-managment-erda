﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Visitor_Manage_;
using Visitor_Manage_.Visitor_List;

namespace Visitor_Manage_
{
    public partial class FrmVisitorDetails : Form
    {
        Timer MyTimer = new Timer();
        public FrmVisitorDetails()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void FrmVisitorDetails_Load(object sender, EventArgs e)
        {
            groupBox1.Left = (this.ClientSize.Width - groupBox1.Width) / 2;
            groupBox1.Top = (this.ClientSize.Height - groupBox1.Height) / 2;
            panel3.Left = (groupBox1.ClientSize.Width - panel3.Width) / 2;

            GEtdetails();
            Fun.VMsg("Welcome");
            Fun.VMsg(LbName.Text);


           MyTimer.Interval = (1 * 60 * 1000); // 20 mins
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void GEtdetails()
        {
            
            VGP_Service ss = new VGP_Service();
            ss.Credentials = Busness.ICredentials();

            List<VGP_Filter> L = new List<VGP_Filter>();
            VGP_Filter VLF = new VGP_Filter();
            VLF.Field = VGP_Fields.Visitor_No;
            VLF.Criteria = Busness.VisitorNo;
            L.Add(VLF);

            Visitor_List.VGP[] Rec = ss.ReadMultiple(L.ToArray(), null, 1);
            if (Rec.Length > 0)
            {
                foreach (Visitor_List.VGP v in Rec)
                {
                    LbName.Text = v.Name;
                    LBContactNo.Text = v.Contact_No;
                    LBAddress.Text = v.Address;
                    LBAddress2.Text = v.Address_2;
                    LBEmailID.Text = v.E_Mail;
                    LBCOMPNAME.Text = v.Company_Name;
                    LBCmpAddress.Text = v.Company_Address;
                    LBDepartment.Text = v.Department;
                    LBWgoomMeet.Text = v.Whom_to_Meet;
                    LBPurpose.Text = v.Purpose_Meeting_Witness;


                }

            }
           

        }

        private void LbName_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FRM_IN vds = new FRM_IN();
            vds.ShowDialog(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            CaptureImg vds = new CaptureImg();
            vds.ShowDialog(this);
            this.Close();
        }
        private void MyTimer_Tick(object sender, EventArgs e)
        {
            MessageBox.Show("The form will now be closed.", "Time Elapsed");
            MyTimer.Stop();
            this.Hide();
            this.Close();
        }
    }
    }

