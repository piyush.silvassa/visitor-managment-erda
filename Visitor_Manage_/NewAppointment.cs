﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Visitor_Manage_.VisitorGatePass_;
using Visitor_Manage_.responsibility_center_list;
using System.Net.Mail;
using Visitor_Manage_.SMTSetup_;
using System.Net;
using Visitor_Manage_.SendMailTmpCust;
using System.Net.Mime;
using System.Diagnostics;

namespace Visitor_Manage_
{
    public partial class NewAppointment : Form
    {
        public NewAppointment()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleparam = base.CreateParams;
                handleparam.ExStyle |= 0x02000000;
                return handleparam;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void Appointment_Load(object sender, EventArgs e)
        {
            //tabControl1.Left = (this.ClientSize.Width - tabControl1.Width) / 2;
            //tabControl1.Top = (this.ClientSize.Height - tabControl1.Height) / 5;
            panel3.Visible = false;
            panel1.Visible = true;
           
            panel1.Left = (this.ClientSize.Width - panel1.Width) / 2;
            panel1.Top = (this.ClientSize.Height - panel1.Height) / 4;
            panel2.Visible = false;
           


            // Below two line is for hide tab and border..
            tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tabControl1.Region = new Region(new RectangleF(this.tabPage1.Left, this.tabPage1.Top, this.tabPage1.Width, this.tabPage1.Height));



            // this.tabControl1.Padding = new Point(15, 15);
            //CmbDipartment.Size = new Size(313, 40);
            PlaceHolder();
            LoadDipartment();
        }

       private void PlaceHolder()
        {
            txtName.Text = "Enter Name";
            txtName.ForeColor = Color.Gray;
            txtAddress.Text = "Enter Address";
            txtAddress.ForeColor = Color.Gray;
            txtAddress3.Text = "Enter Address2";
            txtAddress3.ForeColor = Color.Gray;
            txtEmail.Text = "Enter E-Mail";
            txtEmail.ForeColor = Color.Gray;
            txtcontactno.Text = "Enter Contact No";
            txtcontactno.ForeColor = Color.Gray;

            txtcompanyName.Text = "Enter Company Name";
            txtcompanyName.ForeColor = Color.Gray;
            txtCompanyAddress.Text = "Enter Company Address";
            txtCompanyAddress.ForeColor = Color.Gray;
            //txtDepartment.Text = "Enter Department";
            //txtDepartment.ForeColor = Color.Gray;
            txtWhoomtoMeet.Text = "Whoom to Meet";
            txtWhoomtoMeet.ForeColor = Color.Gray;
            txtpurposeofmeeting.Text = "Purpose of Meeting";
            txtpurposeofmeeting.ForeColor = Color.Gray;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectTab(tabPage1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //this.tabControl1.SelectTab(tabPage2);
            panel2.Visible = true;
            panel2.Left = (this.ClientSize.Width - panel2.Width) / 2;
            panel2.Top = (this.ClientSize.Height - panel2.Height) / 4;
            panel1.Visible = false;
            panel3.Visible = false;
            ActiveControl = null;
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //this.tabControl1.SelectTab(tabPage3);
            panel3.Visible = true;

            panel3.Left = (this.ClientSize.Width - panel3.Width) / 2;
            panel3.Top = (this.ClientSize.Height - panel3.Height) / 5;
            panel2.Visible = false;
            panel1.Visible = false;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            CreateApoiment();
            MessageBox.Show("Your Registration has been Completed....");

            Form1 f = new Form1();
            f.Close();

            this.Close();
        }

        private void txtName_Leave(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                txtName.Text = "Enter Name";
                txtName.ForeColor = Color.Gray;
            }
        }

        private void txtName_Enter(object sender, EventArgs e)
        {
            if (txtName.Text == "Enter Name")
            {
                txtName.Text = "";
            }
        }

        private void txtAddress_Leave(object sender, EventArgs e)
        {
            if (txtAddress.Text == "")
            {
                txtAddress.Text = "Enter Address";
                txtAddress.ForeColor = Color.Gray;
            }
        }

        private void txtAddress_Enter(object sender, EventArgs e)
        {
            if (txtAddress.Text == "Enter Address")
            {
                txtAddress.Text = "";
            }
        }

        private void txtAddress3_Leave(object sender, EventArgs e)
        {
            if (txtAddress3.Text == "")
            {
                txtAddress3.Text = "Enter Address2";
                txtAddress3.ForeColor = Color.Gray;
            }
        }

        private void txtAddress3_Enter(object sender, EventArgs e)
        {
            if (txtAddress3.Text == "Enter Address2")
            {
                txtAddress3.Text = "";
            }
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (txtEmail.Text == "")
            {
                txtEmail.Text = "Enter E-Mail";
                txtEmail.ForeColor = Color.Gray;
            }
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            if (txtEmail.Text == "Enter E-Mail")
            {
                txtEmail.Text = "";
            }
        }

        private void txtcontactno_Leave(object sender, EventArgs e)
        {
            if (txtcontactno.Text == "")
            {
                txtcontactno.Text = "Enter Contact No";
                txtcontactno.ForeColor = Color.Gray;
            }
        }

        private void txtcontactno_Enter(object sender, EventArgs e)
        {
            if (txtcontactno.Text == "Enter Contact No")
            {
                txtcontactno.Text = "";
            }
        }

        private void txtcompanyName_Leave(object sender, EventArgs e)
        {
            if (txtcompanyName.Text == "")
            {
                txtcompanyName.Text = "Enter Company Name";
                txtcompanyName.ForeColor = Color.Gray;
            }
        }

        private void txtcompanyName_Enter(object sender, EventArgs e)
        {
            if (txtcompanyName.Text == "Enter Company Name")
            {
                txtcompanyName.Text = "";
            }
        }

        private void txtCompanyAddress_Leave(object sender, EventArgs e)
        {
            if (txtCompanyAddress.Text == "")
            {
                txtCompanyAddress.Text = "Enter Company Address";
                txtCompanyAddress.ForeColor = Color.Gray;
            }
        }

        private void txtCompanyAddress_Enter(object sender, EventArgs e)
        {
            if (txtCompanyAddress.Text == "Enter Company Address")
            {
                txtCompanyAddress.Text = "";
            }
        }

        //private void txtDepartment_Leave(object sender, EventArgs e)
        //{
        //    if (txtDepartment.Text == "")
        //    {
        //        txtDepartment.Text = "Enter Department";
        //        txtDepartment.ForeColor = Color.Gray;
        //    }
        //}

        //private void txtDepartment_Enter(object sender, EventArgs e)
        //{
        //    if (txtDepartment.Text == "Enter Department")
        //    {
        //        txtDepartment.Text = "";
        //    }
        //}

        private void txtWhoomtoMeet_Leave(object sender, EventArgs e)
        {
            if (txtWhoomtoMeet.Text == "")
            {
                txtWhoomtoMeet.Text = "Whoom to Meet";
                txtWhoomtoMeet.ForeColor = Color.Gray;
            }
        }

        private void txtWhoomtoMeet_Enter(object sender, EventArgs e)
        {
            if (txtWhoomtoMeet.Text == "Whoom to Meet")
            {
                txtWhoomtoMeet.Text = "";
            }
        }

        private void txtpurposeofmeeting_Leave(object sender, EventArgs e)
        {
            if (txtpurposeofmeeting.Text == "")
            {
                txtpurposeofmeeting.Text = "Purpose of Meeting";
                txtpurposeofmeeting.ForeColor = Color.Gray;
            }
        }

        private void txtpurposeofmeeting_Enter(object sender, EventArgs e)
        {
            if (txtpurposeofmeeting.Text == "Purpose of Meeting")
            {
                txtpurposeofmeeting .Text = "";
            }
        }

        public void CreateApoiment()
        {
            string visitp="";
            if (radioButton1.Checked)
            {
                visitp = "Visitor";
            }
            if (radioButton2.Checked)
            {
                visitp = "Customer";
            }
            if (radioButton3.Checked)
            {
                visitp = "Vendor";
            }
            if (radioButton4.Checked)
            {
                visitp = "Agent";
            }
            if (radioButton4.Checked)
            {
                visitp = "Witness Testing";
            }
            try
            {

                int selectedIndex = CmbDipartment.SelectedIndex;
                CmbDipartment.SelectedItem.ToString();
                var selectedValue = CmbDipartment.Items[selectedIndex];
                var mm = CmbDipartment.SelectedValue;
                
                responsibility_center_list.adcs_responsibility_center_list selectedItem = (responsibility_center_list.adcs_responsibility_center_list)CmbDipartment.Items[CmbDipartment.SelectedIndex];

                VisitorGatePass_Service vs = new VisitorGatePass_Service();
                vs.Credentials = Busness.ICredentials();

                VisitorGatePass vss = new VisitorGatePass();
                //vss.Visitor_Type =Visitor_Type.Agent;
                vss.Name = txtName.Text;
                vss.Address =  txtAddress.Text;
                vss.Address_2 = txtAddress3.Text;
                vss.Company_Name = txtcompanyName.Text;
                vss.Company_Address = txtCompanyAddress.Text;
                vss.Contact_No = txtcontactno.Text;
                vss.Department = selectedItem.Department_Code;
                vss.Purpose_Meeting_Witness = txtpurposeofmeeting.Text;
                vss.Whom_to_Meet = txtWhoomtoMeet.Text;
                vss.E_Mail = txtEmail.Text;
                vs.Create(ref vss);

                SendMailTmpCust.Send_Mail_Tmpcust Se = new SendMailTmpCust.Send_Mail_Tmpcust();
                Se.Credentials = Busness.ICredentials();
                Se.VisitorSetType(vss.Visitor_No, visitp);
                // EmailGenerate("piyush.silvassa@gmail.com", vss.Name, vss.Contact_No, vss.Visitor_No);

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
        public  void LoadDipartment()
        {
            try
            {
                 adcs_responsibility_center_list_Service srv = new adcs_responsibility_center_list_Service();
                srv.Credentials = Busness.ICredentials();
                List<adcs_responsibility_center_list_Filter> MCFL = new List<adcs_responsibility_center_list_Filter>();
                adcs_responsibility_center_list_Filter CFL = new adcs_responsibility_center_list_Filter();
                //CFL.Field = Users_List_Fields.User_ID;
                //CFL.Criteria = "*";
                //MCFL.Add(CFL);

                responsibility_center_list.adcs_responsibility_center_list [] Rec = srv.ReadMultiple(MCFL.ToArray(), null, 1000);
                if (Rec.Length > 0)
                {
                   // Rec[].HRSection_Name
                    CmbDipartment.DataSource = Rec;
                    CmbDipartment.DisplayMember = "HRSection_Name";
                    CmbDipartment.ValueMember = "Code";
                   // CmbDipartment.DataBind();

                }
            }
            catch (Exception e)
            {
            }
        }

        protected void EmailGenerate(string toaddress, string Name, String Mobile1, string VisitorNo)
        {
            SMTP_Setup_Service Smail = new SMTP_Setup_Service();
            Smail.Credentials = Busness.ICredentials();
            List<SMTP_Setup_Filter> fl = new List<SMTP_Setup_Filter>();
            SMTP_Setup[] RR = Smail.ReadMultiple(fl.ToArray(), null, 1);

            SendMailTmpCust.Send_Mail_Tmpcust Se = new SendMailTmpCust.Send_Mail_Tmpcust();
            Se.Credentials = Busness.ICredentials();

            SmtpClient smtp = new SmtpClient(RR[0].SMTP_Server);
            string myGmailAddress = RR[0].User_ID;
            string appSpecificPassword = RR[0].Password;
            smtp.EnableSsl = false;
            smtp.Port = RR[0].SMTP_Server_Port;
            smtp.Credentials = new NetworkCredential(myGmailAddress, appSpecificPassword);
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(myGmailAddress, "ERDA");
            message.From = new MailAddress(myGmailAddress, "ERDA");
            message.To.Add(new MailAddress(toaddress, "Name"));
            message.IsBodyHtml = true;

            //string VisitorNo= "";
            //SendMailTmpCust Se1 = new SendMailTmpCust();
            //Se1.Credentials = ErdaCredentials.ICredentials();
            //VisitorNo = Se1.GetVisitorNo();

            string mailbody = "<html><body>Dear Visitor " + Name + ",<br><P> Your Visitor No." + VisitorNo + "</p><p>check your QR code</p><img src=\"cid:Email\"  width='100' height='100'></body></html>";
            AlternateView AlternateView_Html = AlternateView.CreateAlternateViewFromString(mailbody, null, MediaTypeNames.Text.Html);
            // Create a LinkedResource object and set Image location path and Type
            LinkedResource Picture1 = new LinkedResource("QR" + Convert.ToString(Mobile1) + ".jpg", MediaTypeNames.Image.Jpeg);
            Picture1.ContentId = "Email";
            AlternateView_Html.LinkedResources.Add(Picture1);
            message.AlternateViews.Add(AlternateView_Html);

            message.Subject = "Visitor Registration";
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            smtp.Send(message);
            //try
            //{
            //    smtp.Send(message);

            //}

            //catch (Exception ex)
            //{
            //    throw ex;
            //}


        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAddress_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtName_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtAddress3_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtEmail_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtcontactno_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtcompanyName_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtCompanyAddress_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtWhoomtoMeet_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void txtpurposeofmeeting_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(new ProcessStartInfo(

        ((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void button6_Click(object sender, EventArgs e)
        {
           // MyTimer.Stop();
            this.Hide();
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //this.tabControl1.SelectTab(tabPage1);

            panel1.Visible = true;
            panel1.Left = (this.ClientSize.Width - panel1.Width) / 2;
            panel1.Top = (this.ClientSize.Height - panel1.Height) / 4;
            panel2.Visible = false;
            panel3.Visible = false;
            ActiveControl = null;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //this.tabControl1.SelectTab(tabPage2);

            panel2.Visible = true;
            panel2.Left = (this.ClientSize.Width - panel2.Width) / 2;
            panel2.Top = (this.ClientSize.Height - panel2.Height) / 4;
            panel3.Visible = false;
            panel1.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                CreateApoiment();
              //  MessageBox.Show("Your Registration has been Completed....");

                Form1 f = new Form1();
                f.Close();

                this.Close();
            }
            catch (Exception)
            {

                throw;
            }
          
        }
    }
}
